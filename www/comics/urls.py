from django.urls import path

from .views import ComicListView

urlpatterns = [
    path('', ComicListView.as_view(), name='detail'),
]

