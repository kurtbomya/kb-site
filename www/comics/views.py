from .models import Comic
from django.views.generic.list import ListView


class ComicListView(ListView):
    model = Comic
    template_name = 'comics/template.html'
    context_object_name = 'comics'
    paginate_by = 1
    queryset = Comic.objects.all()

    # Debugging what we're sending to the template
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context
