from django.db import models


class Comic(models.Model):
    title = models.CharField(max_length=30, blank=False)
    program = models.TextField(blank=False)

    def __str__(self):
        return self.title
