from .models import MinecraftNotice
from django.views.generic.list import ListView


class MinecraftListView(ListView):
    model = MinecraftNotice
    template_name = 'minecraft/template.html'
    context_object_name = 'minecraftnotices'
    queryset = MinecraftNotice.objects.all()
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context
