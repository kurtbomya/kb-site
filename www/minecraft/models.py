from django.db import models


class MinecraftNotice(models.Model):
    title = models.CharField(max_length=30, blank=False)
    timestamp = models.DateField(auto_now=True)
    notice = models.TextField(blank=False)

    def __str__(self):
        return self.title
