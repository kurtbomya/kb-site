from django.urls import path

from .views import MinecraftListView

urlpatterns = [
    path('', MinecraftListView.as_view(), name='detail'),
]

