def nav(request):
    request_path_info = request.META['PATH_INFO']
    active_app = request_path_info.split('/')[1]
    if active_app == "minecraft":
        context = {'comic': '', 'mc': 'active'}
    elif active_app == "comics":
        context = {'comic': 'active', 'mc': ''}
    else:
        context = {'comic': '', 'mc': ''}
    return context
